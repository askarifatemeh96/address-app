
// function test() {

//     var app = new Mapp({
//         element: '#app',
//         presets: {
//             latlng: {
//                 lat: 32,
//                 lng: 52,
//             },
//             zoom: 6,
//         },
//         apiKey: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU4MDg2YjEyODA2NzZjYjE4ZTA4MTkwOTRkMjM4NDg3MjNhMGMzODM5Mzg0NDk2NTRkNWM5NzIzYmE4YTMyY2RkZWMxODFhOWVkYzVmNzlkIn0.eyJhdWQiOiIxMDU2OSIsImp0aSI6IjU4MDg2YjEyODA2NzZjYjE4ZTA4MTkwOTRkMjM4NDg3MjNhMGMzODM5Mzg0NDk2NTRkNWM5NzIzYmE4YTMyY2RkZWMxODFhOWVkYzVmNzlkIiwiaWF0IjoxNTk4MTYyMzI3LCJuYmYiOjE1OTgxNjIzMjcsImV4cCI6MTYwMDg0NDMyNywic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.ZdepgRu62B6su7hHguqCUYMOdSTIrmdj6xCIn8tSjNSA5VxdonnGG6R5ukm17PDoBsoIvamCf2Tyez8oWdWKipBMjA6AtVBBLOBa5GiocydL4tWUokXVNC4tHIx2nw6nBy3CsC8P9n2etJdwXBerD6_B0OqDCPhx9oFaa7J-8vX62RkeXRtwwmP4fMgpxaw3Fc3LmGdRYTOYKBDiG5SFV81yRif8sm1Pz0x9EJZYGK7zMT1kJGK6go0NbVPSuL8RqkNVzdu-zJwVs4emhqnki3K8S15JVGAQSXkh4Yzou8dIWO0EmjwRziagPIiKcrCRQqvddPAstvYLfylMtIsRFg'
//     });

//     app.addLayers();
//     var marker = app.addMarker({
//         name: 'advanced-marker',
//         latlng: {
//             lat: 37.375,
//             lng: 49.759,
//         },
//         icon: app.icons.red,
//         popup: {
//             title: {
//                 i18n: 'marker-title',
//             },
//             description: {
//                 i18n: 'marker-description',
//             },
//             class: 'marker-class',
//             open: true,
//         },
//         pan: false,
//         draggable: true,
//         history: false,
//     });

//     marker.on('dragend', function (event) {
//         var position = marker.getLatLng();
//         marker.setLatLng(position, {
//             draggable: 'true'
//         }).bindPopup(position).update();
//         console.log(marker.getLatLng());
//     })
// }

function test() {
    var app = new Mapp({
        element: '#app',
        presets: {
            latlng: {
                lat: 32,
                lng: 52,
            },
            zoom: 6,
        },
        apiKey: 'Your API Key'
    });
    app.addLayers();
};


// $(document).ready(function () {
//     var app = new Mapp({
//         element: '#app',
//         presets: {
//             latlng: {
//                 lat: 32,
//                 lng: 52,
//             },
//             zoom: 6
//         },
//         i18n: {
//             fa: {
//                 'marker-title': 'عنوان',
//                 'marker-description': 'توضیح',
//             },
//             en: {
//                 'marker-title': 'Title',
//                 'marker-description': 'Description',
//             },
//         },
//         locale: 'en',
//         apiKey: 'Your API Key'
//     });
//     app.addLayers();
//     var marker = app.addMarker({
//         name: 'advanced-marker',
//         latlng: {
//             lat: 37.375,
//             lng: 49.759,
//         },
//         icon: app.icons.red,
//         popup: {
//             title: {
//                 i18n: 'marker-title',
//             },
//             description: {
//                 i18n: 'marker-description',
//             },
//             // custom: 'Custom popup',
//             class: 'marker-class',
//             open: true,
//         },
//         pan: false,
//         draggable: true,
//         history: false,
//         on: {
//             click: function () {
//                 console.log('Click callback');
//             },
//             contextmenu: function () {
//                 console.log('Contextmenu callback');
//             },
//         },
//     });
// });