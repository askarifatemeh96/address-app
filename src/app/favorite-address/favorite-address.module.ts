import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FavoriteAddressRoutingModule } from './favorite-address-routing.module';
import { FavoriteAddressCardComponent } from './Components/favorite-address-card/favorite-address-card.component';
import { FavoriteContentComponent } from './Components/favorite-content/favorite-content.component';
import { AddFavoriteAddressCardComponent } from './Components/add-favorite-address-card/add-favorite-address-card.component'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { EditFavoriteAddressCardComponent } from './Components/edit-favorite-address-card/edit-favorite-address-card.component';


@NgModule({
  declarations: [FavoriteAddressCardComponent,
    FavoriteContentComponent,
    AddFavoriteAddressCardComponent,
    EditFavoriteAddressCardComponent],
  imports: [
    CommonModule,
    FavoriteAddressRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class FavoriteAddressModule { }
