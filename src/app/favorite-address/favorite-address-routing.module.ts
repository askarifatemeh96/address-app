import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FavoriteContentComponent } from './Components/favorite-content/favorite-content.component';
import { AddFavoriteAddressCardComponent } from './Components/add-favorite-address-card/add-favorite-address-card.component'
import { EditFavoriteAddressCardComponent } from './Components/edit-favorite-address-card/edit-favorite-address-card.component'

const routes: Routes = [
  { path: '', component: FavoriteContentComponent },
  { path: 'add', component: AddFavoriteAddressCardComponent },
  { path: 'edit/:id', component: EditFavoriteAddressCardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FavoriteAddressRoutingModule { }
