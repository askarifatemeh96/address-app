import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { FavoriteAddressService } from '../../../Services/favorite-address.service';


@Component({
  selector: 'app-favorite-address-card',
  templateUrl: './favorite-address-card.component.html',
  styleUrls: ['./favorite-address-card.component.scss']
})
export class FavoriteAddressCardComponent implements OnInit {

  constructor(private favoriteAddressService: FavoriteAddressService) { }

  @Input() addressCardID: Number;
  @Input() mood: string;
  @Output() deleteAddressID = new EventEmitter<Number>();
  addressDetails: any;


  ngOnInit(): void {
    this.getFavoriteAddressDetails(this.addressCardID);
  }

  ngOnChanges(): void {
    this.getFavoriteAddressDetails(this.addressCardID);
  }

  getFavoriteAddressDetails(id: Number) {
    this.favoriteAddressService.getFavoriteAddressDetails(id)
      .subscribe((data: any) => {
        this.addressDetails = data;
      })
  }

  deleteFavoriteAddress(id: Number) {
    this.favoriteAddressService.deleteFavoriteAddressDetails(id).subscribe((data: any) => { this.addressDetails = data; });
  }

  deleteAddress(id: Number) {
    this.deleteAddressID.emit(id);
    this.deleteFavoriteAddress(this.addressDetails.id)
  }
}
