import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteAddressCardComponent } from './favorite-address-card.component';

describe('FavoriteAddressCardComponent', () => {
  let component: FavoriteAddressCardComponent;
  let fixture: ComponentFixture<FavoriteAddressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteAddressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteAddressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
