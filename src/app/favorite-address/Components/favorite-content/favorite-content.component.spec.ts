import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteContentComponent } from './favorite-content.component';

describe('FavoriteContentComponent', () => {
  let component: FavoriteContentComponent;
  let fixture: ComponentFixture<FavoriteContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
