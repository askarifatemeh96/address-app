import { Component, OnInit, OnChanges } from '@angular/core';
import { FavoriteAddressService } from '../../../Services/favorite-address.service'
import { AuthService } from '../../../Services/auth.service'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-favorite-content',
  templateUrl: './favorite-content.component.html',
  styleUrls: ['./favorite-content.component.scss']
})
export class FavoriteContentComponent implements OnInit, OnChanges {

  constructor(private favoriteAddressService: FavoriteAddressService,
    private auth: AuthService) { }

  addressList: any[] = []
  ngOnInit(): void {
    this.getFavoriteAddresslist();
  }

  ngOnChanges(): void {
    this.getFavoriteAddresslist();
  }

  getFavoriteAddresslist() {
    this.favoriteAddressService.getFavoriteAddressList(this.auth.getUserId())
      .subscribe((data: any) => {
        this.addressList = data;
      });
  }

  deleteAddressFromView(addressId: Number) {
    for (let i = 0; this.addressList.length - 2; i++) {
      if (addressId === this.addressList[i].id) {
        this.addressList.splice(i, 1);
      }
    }
  }

}


