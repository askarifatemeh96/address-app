import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFavoriteAddressCardComponent } from './edit-favorite-address-card.component';

describe('EditFavoriteAddressCardComponent', () => {
  let component: EditFavoriteAddressCardComponent;
  let fixture: ComponentFixture<EditFavoriteAddressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFavoriteAddressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFavoriteAddressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
