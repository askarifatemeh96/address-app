import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { FavoriteAddressService } from '../../../Services/favorite-address.service'
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-edit-favorite-address-card',
  templateUrl: './edit-favorite-address-card.component.html',
  styleUrls: ['./edit-favorite-address-card.component.scss']
})
export class EditFavoriteAddressCardComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private favoriteAddressService: FavoriteAddressService,
    private Rout: ActivatedRoute,
    private auth: AuthService) { }

  editAddress: FormGroup;
  editAddressId: number;
  detailsBeforeEdit: any;

  ngOnInit(): void {
    this.initForm();
    this.getAddressId();
    this.getFavoriteAddressDetails(this.editAddressId);
  }

  initForm() {
    this.editAddress = this.formBuilder.group({
      name: [null, Validators.required],
      address: [null, Validators.required]
    });
  }

  getAddressId() {
    this.Rout.params.subscribe(
      params => {
        this.editAddressId = params.id;
      })
  }

  getFavoriteAddressDetails(id: Number) {
    this.favoriteAddressService.getFavoriteAddressDetails(id)
      .subscribe((data: any) => {
        this.detailsBeforeEdit = data;
        this.editAddress.patchValue({
          name: this.detailsBeforeEdit.name,
          address: this.detailsBeforeEdit.address
        })
      })
  }

  submit() {
    if (!this.editAddress.valid) return;
    this.editAddress.value['userId'] = this.auth.getUserId();
    this.putFavoriteAddress(this.editAddress.value);
  }

  putFavoriteAddress(address: any) {
    this.favoriteAddressService.putFavoriteAddressDetails(this.editAddressId, address).subscribe();
  }

}