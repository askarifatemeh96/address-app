import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { FavoriteAddressService } from '../../../Services/favorite-address.service'
import { AuthService } from '../../../Services/auth.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-favorite-address-card',
  templateUrl: './add-favorite-address-card.component.html',
  styleUrls: ['./add-favorite-address-card.component.scss']
})
export class AddFavoriteAddressCardComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private favoriteAddressService: FavoriteAddressService,
    private auth: AuthService,
    private router: Router) { }

  addAddress: FormGroup;

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.addAddress = this.formBuilder.group({
      name: [null, Validators.required],
      address: [null, Validators.required]
    });
  }

  submit() {
    if (!this.addAddress.valid) return;
    this.addAddress.value['userId'] = this.auth.getUserId();
    this.postFavoriteAddress(this.addAddress.value);
  }

  postFavoriteAddress(address: any) {
    this.favoriteAddressService.postFavoriteAddressDetails(address).subscribe(() => { this.router.navigate(['favorite-addresses']); })
  }
}
