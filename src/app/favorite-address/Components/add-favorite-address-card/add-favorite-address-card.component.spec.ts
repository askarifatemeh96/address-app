import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFavoriteAddressCardComponent } from './add-favorite-address-card.component';

describe('AddFavoriteAddressCardComponent', () => {
  let component: AddFavoriteAddressCardComponent;
  let fixture: ComponentFixture<AddFavoriteAddressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFavoriteAddressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFavoriteAddressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
