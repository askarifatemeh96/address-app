import { Injectable } from '@angular/core';
import { AuthService } from './Services/auth.service';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable()

export class AuthGuard implements CanActivate {

    constructor(private auth: AuthService,
        private router: Router) { }

    public canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.auth.isloggedIn()) {
            this.router.navigate(['login']);
            return false;
        }
        return true;
    }

}