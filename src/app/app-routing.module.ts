import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './authGuard'

const routes: Routes = [
  { path: '', loadChildren: () => import('./public-address/public-address.module').then(m => m.PublicAddressModule) },
  { path: 'public-addresses', loadChildren: () => import('./public-address/public-address.module').then(m => m.PublicAddressModule) },
  { path: 'favorite-addresses', loadChildren: () => import('./favorite-address/favorite-address.module').then(m => m.FavoriteAddressModule), canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', loadChildren: () => import('./public-address/public-address.module').then(m => m.PublicAddressModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
