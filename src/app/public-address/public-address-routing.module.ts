import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './Components/content/content.component'
import { AddAddressCardComponent } from './Components/add-address-card/add-address-card.component';
import { EditAddressCardComponent } from './Components/edit-address-card/edit-address-card.component';


const routes: Routes = [
    { path: '', component: ContentComponent, data: { type: 'public' } },
    { path: 'add', component: AddAddressCardComponent, data: { type: 'public' } },
    { path: 'edit/:id', component: EditAddressCardComponent, data: { type: 'public' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PublicAddressRoutingModule { }
