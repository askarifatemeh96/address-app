import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAddressCardComponent } from './add-address-card.component';

describe('AddAddressCardComponent', () => {
  let component: AddAddressCardComponent;
  let fixture: ComponentFixture<AddAddressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAddressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAddressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
