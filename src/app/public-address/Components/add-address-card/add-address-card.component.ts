import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AddressServiceService } from '../../../Services/address-service.service'
import { Router } from '@angular/router';
declare const test;

@Component({
  selector: 'app-add-address-card',
  templateUrl: './add-address-card.component.html',
  styleUrls: ['./add-address-card.component.scss']
})
export class AddAddressCardComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,
    private addressService: AddressServiceService,
    private router: Router) { }

  addAddress: FormGroup;

  ngOnInit(): void {
    this.initForm();
    test();
  }

  initForm() {
    this.addAddress = this.formBuilder.group({
      name: [null, Validators.required],
      address: [null, Validators.required]
    });
  }

  submit() {
    if (!this.addAddress.valid) return;
    this.postPublicAddress(this.addAddress.value);
  }

  postPublicAddress(address: any) {
    this.addressService.postPublicAddressDetails(address)
      .subscribe(() => { this.router.navigate(['public-addresses']); })
  }
}
