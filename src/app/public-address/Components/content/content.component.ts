import { Component, OnInit, OnChanges } from '@angular/core';
import { AddressServiceService } from '../../../Services/address-service.service'

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnChanges {

  constructor(private addressService: AddressServiceService) { }

  addressList: any[] = []

  ngOnInit(): void {
    this.getPublicAddresslist();
  }

  ngOnChanges(): void {
    this.getPublicAddresslist();
  }

  getPublicAddresslist() {
    this.addressService.getPublicAddressList()
      .subscribe((data: any) => {
        this.addressList = data;
      });
  }

  deleteAddressFromView(addressId: Number) {
    for (let i = 0; this.addressList.length; i++) {
      if (addressId === this.addressList[i].id) {
        this.addressList.splice(i, 1);
      }
    }
  }

}

