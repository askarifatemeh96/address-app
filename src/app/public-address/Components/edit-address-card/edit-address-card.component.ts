import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AddressServiceService } from '../../../Services/address-service.service'

@Component({
  selector: 'app-edit-address-card',
  templateUrl: './edit-address-card.component.html',
  styleUrls: ['./edit-address-card.component.scss']
})

export class EditAddressCardComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private addressService: AddressServiceService,
    private Rout: ActivatedRoute) { }

  editAddress: FormGroup;
  editAddressId: number;
  detailsBeforeEdit: any;

  ngOnInit(): void {
    this.initForm();
    this.getAddressId();
    this.getpublicAddressDetails(this.editAddressId);
  }

  initForm() {
    this.editAddress = this.formBuilder.group({
      name: [null, Validators.required],
      address: [null, Validators.required]
    });
  }

  getAddressId() {
    this.Rout.params.subscribe(
      params => {
        this.editAddressId = params.id;
      })
  }

  getpublicAddressDetails(id: Number) {
    this.addressService.getPublicAddressDetails(id)
      .subscribe((data: any) => {
        this.detailsBeforeEdit = data;
        this.editAddress.patchValue({ name: this.detailsBeforeEdit.name, address: this.detailsBeforeEdit.address })
      })
  }

  submit() {
    if (!this.editAddress.valid) return;
    this.putPublicAddress(this.editAddress.value);
  }

  putPublicAddress(address: any) {
    this.addressService.putPublicAddressDetails(this.editAddressId, address).subscribe();
  }
  
}