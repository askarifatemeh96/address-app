import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAddressCardComponent } from './edit-address-card.component';

describe('EditAddressCardComponent', () => {
  let component: EditAddressCardComponent;
  let fixture: ComponentFixture<EditAddressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAddressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAddressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
