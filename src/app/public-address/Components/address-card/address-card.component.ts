import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { AddressServiceService } from '../../../Services/address-service.service';

@Component({
  selector: 'app-address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss']
})
export class AddressCardComponent implements OnInit, OnChanges {

  constructor(private addressService: AddressServiceService) { }

  @Input() addressCardID: Number;
  @Input() mood: string;
  @Output() deleteAddressID = new EventEmitter<Number>();
  addressDetails: any;

  ngOnInit(): void {
    this.getPublicAddressDetails(this.addressCardID);
  }

  ngOnChanges(): void {
    this.getPublicAddressDetails(this.addressCardID);
  }

  getPublicAddressDetails(id: Number) {
    this.addressService.getPublicAddressDetails(id)
      .subscribe((data: any) => {
        this.addressDetails = data;
      })
  }

  deletePublicAddress(id: Number) {
    this.addressService.deletePublicAddressDetails(id).subscribe((data: any) => { this.addressDetails = data });
  }

  deleteAddress(id: Number) {
    this.deleteAddressID.emit(id);
    this.deletePublicAddress(this.addressDetails.id)
  }

}
