import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddressServiceService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  PublicAddressDetailsURL = 'http://localhost:3000/public-addresses/'


  getPublicAddressList(): Observable<any> {
    return this.http.get<any>(`${this.PublicAddressDetailsURL}`)
  }

  getPublicAddressDetails(id: Number): Observable<any> {
    return this.http.get<any>(`${this.PublicAddressDetailsURL}${id}`)
  }

  putPublicAddressDetails(id: Number, address: any) {
    return this.http.put<any>(`${this.PublicAddressDetailsURL}${id}`, address)
  }

  postPublicAddressDetails(address: any) {
    return this.http.post<any>(`${this.PublicAddressDetailsURL}`, address)
      .pipe(
        catchError(this.handleError)
      );
  }

  deletePublicAddressDetails(id: Number) {
    return this.http.delete<any>(`${this.PublicAddressDetailsURL}${id}`)
      .pipe(
        catchError(this.handleError)
      );
  }

}

