import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  setToken(tokenKey, tokenValue) {
    localStorage.setItem(tokenKey, tokenValue)
  }

}
