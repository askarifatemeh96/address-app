import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class FavoriteAddressService {

  constructor(private http: HttpClient,
    private auth: AuthService) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  FavoriteAddressDetailsURL = 'http://localhost:3000/favorite-addresses'


  getFavoriteAddressList(userId: number): Observable<any> {
    return this.http.get<any>(`${this.FavoriteAddressDetailsURL}?userId=${userId}`)
  }

  getFavoriteAddressDetails(id: Number): Observable<any> {
    return this.http.get<any>(`${this.FavoriteAddressDetailsURL}/${id}`)
  }

  putFavoriteAddressDetails(id: Number, address: any) {
    return this.http.put<any>(`${this.FavoriteAddressDetailsURL}/${id}`, address)
  }

  postFavoriteAddressDetails(address: any) {
    return this.http.post<any>(`${this.FavoriteAddressDetailsURL}`, address)
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteFavoriteAddressDetails(id: Number) {
    return this.http.delete<any>(`${this.FavoriteAddressDetailsURL}/${id}`)
      .pipe(
        catchError(this.handleError)
      );
  }
}
