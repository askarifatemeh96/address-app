import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loginURL = 'http://localhost:3000/login';
  registerURL = 'http://localhost:3000/register'
  userInfo = 'http://localhost:3000/users'
  constructor(private http: HttpClient) { }

  postLoginInfo(user: any) {
    return this.http.post<any>(`${this.loginURL}`, user)
  }

  postRegisterInfo(user: any): Observable<any> {
    return this.http.post<any>(`${this.registerURL}`, user)
  }

  getUserInfo(userId: number) {
    return this.http.get<any>(`${this.userInfo}/${userId}`)
  }
}
