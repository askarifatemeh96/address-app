import { Injectable } from '@angular/core';
import { UserService } from './user.service'
import { Router } from '@angular/router';
import * as JWT from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private userService: UserService,
    private router: Router) { }

  token: string
  decoded: any

  isloggedIn() {
    if (!localStorage.getItem('token')) return false;
    else return true;
  }

  logOut() {
    localStorage.setItem('token', '')
    localStorage.setItem('email', '')
    localStorage.setItem('userId', '')
  }

  logIn(loginInfo: any) {
    this.userService.postLoginInfo(loginInfo).subscribe((data: any) => {
      this.token = data.accessToken;
      this.decoded = JWT(this.token);
      localStorage.setItem('token', this.token);
      localStorage.setItem('email', this.decoded['email']);
      localStorage.setItem('userId', this.decoded['sub']);
      alert("welcome dear " + this.decoded.email);
      this.router.navigate([''])
    })
  }

  register(registerInfo: any) {
    this.userService.postRegisterInfo(registerInfo).subscribe((data: any) => {
      this.token = data.accessToken;
      this.decoded = JWT(this.token);
      localStorage.setItem('token', this.token);
      localStorage.setItem('email', this.decoded['email']);
      localStorage.setItem('userId', this.decoded['sub']);
      alert("welcome dear " + this.decoded.email);
      this.router.navigate(['public-addresses'])
    })
  }

  getEmail() {
    return localStorage.getItem('email');
  }

  getUserId() {
    return parseInt(localStorage.getItem('userId'));
  }

}
