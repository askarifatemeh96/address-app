import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { MediaMatcher } from '@angular/cdk/layout';

import { UserService } from './Services/user.service';
import { AuthService } from './Services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Address-app';
  email: string;
  userId: number;
  name: string;

  navItem = [
    { name: 'Public Addresses', routing: 'public-addresses' },
    { name: 'Add Public Addresses', routing: 'public-addresses/add' },
    { name: 'Favorite Addresses', routing: 'favorite-addresses' },
    { name: 'Add Favorite Addresses', routing: 'favorite-addresses/add' }
  ]

  ngOnInit(): void {
    this.getEmail();
    this.getUserId();
    this.getUserInfo();
  }

  ngOnChanges(): void {
    this.getEmail();
    this.getUserId();
    this.getUserInfo();
  }
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private auth: AuthService,
    private user: UserService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logOut() {
    this.auth.logOut();
  }

  isloggedIn(): boolean {
    return this.auth.isloggedIn();
  }

  getEmail() {
    this.email = this.auth.getEmail();
  }

  getUserId() {
    this.userId = this.auth.getUserId();
  }

  getUserInfo() {
    this.user.getUserInfo(this.userId)
      .subscribe((data: any) => {
        this.name = data.name;
        this.email = data.email;
      })
  }
}
